<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;
use Log;

class TestController extends Controller
{
    public function send(Request $request)
    {
        $client = new Client(new Version2X('http://socketio-server.test:3000'));
        
        $client->initialize();
        
        $client->emit('server:test', [
            'message' => $request->message
        ]);

        $client->close();

        return back();
    }
}
